import {expect} from 'chai';
import * as utils from '../../../src/utils/utils';
import { Trk } from '../../../src/models/Trk';
import { xmldata } from '../fixtures/sample';

const parser = new DOMParser();

describe('Utils', () => {
  it('XMLElementToObject - test 1', () => {
    const data = '<name>WRK</name>';
    const expected = {
      name: 'WRK'
    };
    const gpxDOM = parser.parseFromString(data, 'text/xml') as XMLDocument;
    const dataXML = gpxDOM.getElementsByTagName('name');
    const result = utils.XMLElementToObject(dataXML[0]);
    expect(result).to.to.deep.equal(expected);
  });

  it('XMLElementToObject - test 2', () => {
    const data = '<name type="1">WRK</name>';
    const expected = {
      name: {
        '@type': '1',
        '_value': 'WRK'
      }
    };
    const gpxDOM = parser.parseFromString(data, 'text/xml') as XMLDocument;
    const dataXML = gpxDOM.getElementsByTagName('name');
    const result = utils.XMLElementToObject(dataXML[0]);
    expect(result).to.to.deep.equal(expected);
  });

  it('XMLElementToObject - test 3', () => {
    const data = `<name>
      <example>
        <data>1</data>
      </example>
      <example>
        <data>2</data>
      </example>
    </name>`;
    const expected = {
      name: {
        example: [
          {data: '1'},
          {data: '2'}
        ]
      }
    };
    const gpxDOM = parser.parseFromString(data, 'text/xml') as XMLDocument;
    const dataXML = gpxDOM.getElementsByTagName('name');
    const result = utils.XMLElementToObject(dataXML[0]);
    expect(result).to.to.deep.equal(expected);
  });

  it('XMLElementToObject - test 4', () => {
    const data = `<name type="foo">
      <example>
        <data>1</data>
      </example>
      <example>
        <data>2</data>
      </example>
    </name>`;
    const expected = {
      name: {
        '@type': 'foo',
        'example': [
          {data: '1'},
          {data: '2'}
        ]
      }
    };
    const gpxDOM = parser.parseFromString(data, 'text/xml') as XMLDocument;
    const dataXML = gpxDOM.getElementsByTagName('name');
    const result = utils.XMLElementToObject(dataXML[0]);
    expect(result).to.to.deep.equal(expected);
  });

  it('splitTrksegByTime - two parts', () => {
    const gpxDOM = parser.parseFromString(xmldata, 'text/xml') as XMLDocument;
    const trkXML = gpxDOM.getElementsByTagName('trk');
    const trkJS = utils.XMLElementToObject(trkXML[0]);
    const trk = new Trk(trkJS.trk as any);
    // when
    const result = utils.splitTrksegByTime(trk.trkseg);
    // then
    expect(result.length).to.to.equal(2);
  });

});
