export const xmldata = `<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
    <time>2018-07-10T05:23:24.000Z</time>
  </metadata>
  <trk>
    <name>WRK</name>
    <type>speed walking</type>
    <trkseg>
      <trkpt lat="48.6907586641609668731689453125" lon="21.27528483979403972625732421875">
        <ele>175.1999969482421875</ele>
        <time>2016-06-09T06:39:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>23.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>63</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.6907586641609668731689453125" lon="21.27528483979403972625732421875">
        <ele>175.1999969482421875</ele>
        <time>2016-06-09T06:39:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>23.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>63</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.6907586641609668731689453125" lon="21.27528483979403972625732421875">
        <ele>175.1999969482421875</ele>
        <time>2016-06-09T08:39:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>23.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>63</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.6907586641609668731689453125" lon="21.27528483979403972625732421875">
        <ele>175.1999969482421875</ele>
        <time>2016-06-09T08:39:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>23.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>63</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
    </trkseg>
  </trk>
</gpx>`;
