import { TrkPt } from '../../../src/models/TrkPt';
import { expect } from 'chai';

describe('TrkPt', () => {
  it('constructor - good data', () => {
    const data = {
      '@lat': '0.0',
      '@lon': '0.0',
      'ele': '10',
      'time': '2016-06-09T06:39:24.000Z'
    };
    const expected = {
      '@lat': 0.0,
      '@lon': 0.0,
      'ele': 10,
      'time': new Date('2016-06-09T06:39:24.000Z')
    };
    const trkpt = new TrkPt(data);
    expect(trkpt).to.to.deep.equal(expected);
  });

  it('constructor - garmin data', () => {
    const data = {
      '@lat': '48.6907586641609668731689453125',
      '@lon': '21.27528483979403972625732421875',
      'ele': '175.1999969482421875',
      'extensions': {
        'ns3:TrackPointExtension': {
          'ns3:atemp': '23.0',
          'ns3:cad': '63',
          'ns3:hr': '122'
        }
      },
      'time': '2016-06-09T06:39:24.000Z'
    };
    const expected = {
      '@lat': 48.69075866416097,
      '@lon': 21.27528483979404,
      'ele': 175.1999969482422,
      'extensions': {
        'ns3:TrackPointExtension': {
          'ns3:atemp': 23.0,
          'ns3:cad': 63,
          'ns3:hr': 122
        }
      },
      'time': new Date('2016-06-09T06:39:24.000Z')
    };
    const trkpt = new TrkPt(data);
    expect(trkpt).to.to.deep.equal(expected);
  });

  it('constructor - strava data', () => {
    const data = {
      '@lat': '48.6907586641609668731689453125',
      '@lon': '21.27528483979403972625732421875',
      'ele': '175.1999969482421875',
      'extensions': {
        'k2:TrackPointExtension': {
          'k2:cadence': '63',
          'k2:hr': '122'
        }
      },
      'time': '2016-06-09T06:39:24.000Z'
    };
    const expected = {
      '@lat': 48.69075866416097,
      '@lon': 21.27528483979404,
      'ele': 175.1999969482422,
      'extensions': {
        'k2:TrackPointExtension': {
          'k2:cadence': 63,
          'k2:hr': 122
        }
      },
      'time': new Date('2016-06-09T06:39:24.000Z')
    };
    const trkpt = new TrkPt(data);
    expect(trkpt).to.to.deep.equal(expected);
  });

  it('constructor - unknown extension', () => {
    const data = {
      '@lat': '48.6907586641609668731689453125',
      '@lon': '21.27528483979403972625732421875',
      'ele': '175.1999969482421875',
      'extensions': {
        'foo:TrackPointExtension': {
          'foo:cadence': '63',
          'foo:hr': '122'
        }
      },
      'time': '2016-06-09T06:39:24.000Z'
    };
    const expected = {
      '@lat': 48.69075866416097,
      '@lon': 21.27528483979404,
      'ele': 175.1999969482422,
      'extensions': {},
      'time': new Date('2016-06-09T06:39:24.000Z')
    };
    const trkpt = new TrkPt(data as any);
    expect(trkpt).to.to.deep.equal(expected);
  });
});
