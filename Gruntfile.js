// @ts-check
const sass = require('node-sass');
const webpack_config_prod = require('./webpack.config.prod');
const webpack_config_dev = require('./webpack.config');
const karma_config_dev = 'karma.conf.js';
const karma_config_cov = 'karma.conf.cov.js';

module.exports = function(grunt) {
  grunt.initConfig({
    webpack: {
      options: {
        stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
      },
      prod: webpack_config_prod,
      dev: webpack_config_dev
    },
    tslint: {
      dev: {
        options: {
          configuration: 'tslint.json',
          force: true,
          fix: false
        },
        files: {
          src: ['src/**/*.ts', 'src/**/*.tsx']
        }
      },
      prod: {
        options: {
          configuration: 'tslint.json',
          force: false,
          fix: false
        },
        files: {
          src: ['src/**/*.ts', 'src/**/*.tsx']
        }
      }
    },
    karma: {
      dev: {
        configFile: karma_config_dev
      },
      prod: {
        configFile: karma_config_cov,
        reporters: ['dots', 'coverage-istanbul'],
        coverageIstanbulReporter: {
          reports: ['text-summary'],
          fixWebpackSourcePaths: true
        }
      },
      cov: {
        configFile: karma_config_cov
      },
      debug: {
        configFile: karma_config_dev,
        reporters: ['dots'],
        browsers: ['Chrome'],
        singleRun: false,
        webpack: {
          devtool: 'cheap-module-eval-source-map'
        }
      }
    },
    sass: {
      prod: {
        files: {
          'dist/css/style.css': 'src/sass/main.scss'
        },
        options: {
          implementation: sass,
          sourcemap: 'none',
          style: 'compressed'
        }
      },
      dev: {
        files: {
          'dist/css/style.css': 'src/sass/main.scss'
        },
        options: {
          implementation: sass,
          sourceMap: true,
          sourceComments: false
        }
      }
    },
    clean: ['dist/js/*'],
    watch: {
      ts: {
        files: ['src/**/*.ts', 'test/**/*.ts'],
        tasks: ['tslint:dev', 'webpack:dev'],
        options: {
          debounceDelay: 250,
          interrupt: true
        }
      },
      css: {
        files: ['src/**/*.scss'],
        tasks: ['sass:dev'],
        options: {
          debounceDelay: 250
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-webpack');
  grunt.loadNpmTasks('grunt-tslint');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');

  grunt.registerTask('dev', ['sass:dev', 'tslint:dev', 'karma:dev', 'clean', 'webpack:dev', 'watch']);
  grunt.registerTask('prod', ['sass:prod', 'karma:prod', 'clean', 'webpack:prod']);
  grunt.registerTask('default', ['prod']);
};
