// @ts-check
var path = require('path');

/**
 * @type { import('webpack').Configuration }
 */
module.exports = {
  mode: 'development',
  entry: {
    'gpx-split': './src/main.ts'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'source-map',
  watch: false,
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [
      path.resolve(__dirname, 'src'),
      path.resolve(__dirname, 'node_modules')
    ]
  },
  stats: {
    colors: true,
    entrypoints: true
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [{loader: 'awesome-typescript-loader'}]
      }
    ]
  }
};
