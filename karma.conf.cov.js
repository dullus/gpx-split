var webpackConfig = require('./webpack.config.karma');

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai'],
    plugins: [
      'karma-mocha',
      'karma-chai',
      'karma-chrome-launcher',
      'karma-mocha-reporter',
      'karma-webpack',
      'karma-sourcemap-loader',
      'karma-coverage-istanbul-reporter'
    ],
    files: [
      'test/**/*.ts'
    ],
    mime: {
      'text/x-typescript': ['ts','tsx']
    },
    exclude: [],
    preprocessors: {
      'test/**/*.ts': ['webpack', 'sourcemap']
    },
    webpack: {
      mode: webpackConfig.mode,
      module: webpackConfig.module,
      resolve: webpackConfig.resolve,
      devtool: webpackConfig.devtool,
      stats: webpackConfig.stats,
      plugins: webpackConfig.plugins
    },
    reporters: ['progress', 'coverage-istanbul'],
    coverageIstanbulReporter: {
      reports: [ 'text-summary', 'html' ],
      fixWebpackSourcePaths: true,
      dir: './test/coverage/',
      'report-config': {
        html: {
          subdir: '.'
        }
      }
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: true,
    concurrency: Infinity
  });
};
