# gpx-split

Simple tool to split GPX file with more trips to multiple GPX files for each trip.

## Issue

Sometimes you forgot end trip properly in your GPS device. Multiple trips are included in single GPX
and you want split it automatically to more GPX files for each trip separatelly. **GPX-SPLIT** is here for rescue. By default it splits trips when there is 1 hour gap between trackpoints.


