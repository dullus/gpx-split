// @ts-check
var path = require('path');

/**
 * @type { import('webpack').Configuration }
 */
module.exports = {
  mode: 'development',
  entry: {
    'app': './src/main.ts'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  watch: false,
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [
      path.resolve(__dirname, 'src'),
      path.resolve(__dirname, 'node_modules')
    ]
  },
  stats: 'minimal',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: [
          path.resolve(__dirname, 'node_modules')
        ],
        use: {
          loader: 'awesome-typescript-loader',
          query: {
            compilerOptions: {
              inlineSourceMap: true,
              sourceMap: false
            }
          }
        }
      },
      {
        test: /\.ts$/,
        enforce: 'post',
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          /\.spec\.ts$/
        ],
        use: {
          loader: 'istanbul-instrumenter-loader',
          options: {
            esModules: true,
            produceSourceMap: true
          }
        }
      }
    ]
  },
  plugins: []
};
