/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var models_Trk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! models/Trk */ "./src/models/Trk.ts");
/* harmony import */ var utils_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! utils/utils */ "./src/utils/utils.ts");
/* harmony import */ var utils_dropzone__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! utils/dropzone */ "./src/utils/dropzone.ts");
/* harmony import */ var utils_file__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! utils/file */ "./src/utils/file.ts");
/* harmony import */ var views_result__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! views/result */ "./src/views/result.ts");


// import { xmldata } from '../test/src/fixtures/sample';



let result;
let gpxDOM;
Object(utils_dropzone__WEBPACK_IMPORTED_MODULE_2__["dropZone"])(document.getElementById('drop-zone')).then((data) => {
    console.log(`file: ${data.file.name}`);
    // console.log(data);
    result = processTrk(data.data);
    Object(views_result__WEBPACK_IMPORTED_MODULE_4__["drawResult"])(result, stripSuffix(data.file.name));
    assignButtons();
}, (error) => {
    console.log('feed failed ' + error.toString());
});
function stripSuffix(fileName) {
    const parts = fileName.split('.');
    parts.splice(-1, 1);
    return parts.join('.');
}
// load XML file and convert to JS
function processTrk(xmldata) {
    const parser = new DOMParser();
    gpxDOM = parser.parseFromString(xmldata, 'text/xml');
    const trkXML = gpxDOM.getElementsByTagName('trk');
    const trkJS = utils_utils__WEBPACK_IMPORTED_MODULE_1__["XMLElementToObject"](trkXML[0]);
    const trk = new models_Trk__WEBPACK_IMPORTED_MODULE_0__["Trk"](trkJS.trk);
    // split to segments
    const trkSegs = utils_utils__WEBPACK_IMPORTED_MODULE_1__["splitTrksegByTime"](trk.trkseg);
    // console.log(result);
    return trkSegs;
}
function assignButtons() {
    // const buttons: HTMLCollectionOf<HTMLButtonElement> = document.getElementsByClassName('result__download');
    const buttons = document.getElementsByClassName('result__download');
    for (const button of buttons) {
        button.addEventListener('click', (event) => {
            const target = event.currentTarget;
            const segment = target.dataset.segment;
            const fileName = target.dataset.file;
            saveTrkSeg(result[segment], fileName);
        });
    }
}
function saveTrkSeg(trkSeg, fileName = 'out.gpx') {
    // create new XML document
    const myDOM = new Document(); // @TODO not ideal, adds xmlns
    // clone old GPX structure
    const gpxNode = gpxDOM.getElementsByTagName('gpx')[0];
    const gpxClone = gpxNode.cloneNode(true);
    // add it to new XML document
    myDOM.appendChild(gpxClone);
    // remove cloned trackseg
    myDOM.getElementsByTagName('trkseg')[0].remove();
    // add new empty trackseg
    const newTrkseg = document.createElement('trkseg');
    myDOM.getElementsByTagName('trk')[0].appendChild(newTrkseg);
    // add trackpoints
    trkSeg.trkpt.forEach((trkpt) => {
        newTrkseg.appendChild(utils_utils__WEBPACK_IMPORTED_MODULE_1__["js2node"](trkpt, 'trkpt'));
    });
    // console.log(myDOM);
    // XML to string
    const xmlString = new XMLSerializer().serializeToString(myDOM);
    Object(utils_file__WEBPACK_IMPORTED_MODULE_3__["save"])(xmlString, fileName);
    // console.log(result2);
}


/***/ }),

/***/ "./src/models/GpxTpxTrackPointExtension.ts":
/*!*************************************************!*\
  !*** ./src/models/GpxTpxTrackPointExtension.ts ***!
  \*************************************************/
/*! exports provided: GpxTpxTrackPointExtension */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpxTpxTrackPointExtension", function() { return GpxTpxTrackPointExtension; });
class GpxTpxTrackPointExtension {
    constructor(data) {
        this['gpxtpx:hr'] = Number(data['gpxtpx:hr']);
    }
}


/***/ }),

/***/ "./src/models/K2TrackPointExtension.ts":
/*!*********************************************!*\
  !*** ./src/models/K2TrackPointExtension.ts ***!
  \*********************************************/
/*! exports provided: K2TrackPointExtension */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "K2TrackPointExtension", function() { return K2TrackPointExtension; });
class K2TrackPointExtension {
    constructor(data) {
        this['k2:hr'] = Number(data['k2:hr']);
        this['k2:cadence'] = Number(data['k2:cadence']);
    }
}


/***/ }),

/***/ "./src/models/Ns3TrackPointExtension.ts":
/*!**********************************************!*\
  !*** ./src/models/Ns3TrackPointExtension.ts ***!
  \**********************************************/
/*! exports provided: Ns3TrackPointExtension */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ns3TrackPointExtension", function() { return Ns3TrackPointExtension; });
class Ns3TrackPointExtension {
    constructor(data) {
        this['ns3:atemp'] = Number(data['ns3:atemp']);
        this['ns3:hr'] = Number(data['ns3:hr']);
        this['ns3:cad'] = Number(data['ns3:cad']);
    }
}


/***/ }),

/***/ "./src/models/Trk.ts":
/*!***************************!*\
  !*** ./src/models/Trk.ts ***!
  \***************************/
/*! exports provided: Trk */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Trk", function() { return Trk; });
/* harmony import */ var models_TrkSeg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! models/TrkSeg */ "./src/models/TrkSeg.ts");

class Trk {
    constructor(data) {
        this.name = data.name;
        this.type = data.type;
        this.trkseg = new models_TrkSeg__WEBPACK_IMPORTED_MODULE_0__["TrkSeg"](data.trkseg);
    }
}


/***/ }),

/***/ "./src/models/TrkPt.ts":
/*!*****************************!*\
  !*** ./src/models/TrkPt.ts ***!
  \*****************************/
/*! exports provided: TrkPt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrkPt", function() { return TrkPt; });
/* harmony import */ var models_Ns3TrackPointExtension__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! models/Ns3TrackPointExtension */ "./src/models/Ns3TrackPointExtension.ts");
/* harmony import */ var models_K2TrackPointExtension__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! models/K2TrackPointExtension */ "./src/models/K2TrackPointExtension.ts");
/* harmony import */ var models_GpxTpxTrackPointExtension__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! models/GpxTpxTrackPointExtension */ "./src/models/GpxTpxTrackPointExtension.ts");



class TrkPt {
    constructor(data) {
        this['@lat'] = Number(data['@lat']);
        this['@lon'] = Number(data['@lon']);
        this.ele = Number(data.ele);
        if (typeof data.extensions !== 'undefined') {
            this.extensions = {};
            this.setExtension(data.extensions);
        }
        this.time = new Date(data.time);
    }
    setExtension(data) {
        const prefix = Object.keys(data)[0].split(':')[0];
        let ext = data[`ns3:TrackPointExtension`];
        if (prefix === 'ns3' && this.isExtType(ext, 'ns3:')) {
            this.extensions[`ns3:TrackPointExtension`] = new models_Ns3TrackPointExtension__WEBPACK_IMPORTED_MODULE_0__["Ns3TrackPointExtension"](ext);
            return;
        }
        ext = data[`k2:TrackPointExtension`];
        if (prefix === 'k2' && this.isExtType(ext, 'k2:')) {
            this.extensions[`k2:TrackPointExtension`] = new models_K2TrackPointExtension__WEBPACK_IMPORTED_MODULE_1__["K2TrackPointExtension"](ext);
            return;
        }
        ext = data[`gpxtpx:TrackPointExtension`];
        if (prefix === 'gpxtpx' && this.isExtType(ext, 'gpxtpx:')) {
            this.extensions[`gpxtpx:TrackPointExtension`] = new models_GpxTpxTrackPointExtension__WEBPACK_IMPORTED_MODULE_2__["GpxTpxTrackPointExtension"](ext);
            return;
        }
    }
    isExtType(obj, prefix) {
        let valid = true;
        valid = valid && !!obj && typeof obj === 'object';
        Object.keys(obj).forEach((key) => { valid = valid && key.includes(prefix); });
        return valid;
    }
}


/***/ }),

/***/ "./src/models/TrkSeg.ts":
/*!******************************!*\
  !*** ./src/models/TrkSeg.ts ***!
  \******************************/
/*! exports provided: TrkSeg */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrkSeg", function() { return TrkSeg; });
/* harmony import */ var models_TrkPt__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! models/TrkPt */ "./src/models/TrkPt.ts");

class TrkSeg {
    constructor(data) {
        if (data && data.trkpt && Array.isArray(data.trkpt)) {
            this.trkpt = data.trkpt.map((t) => {
                return new models_TrkPt__WEBPACK_IMPORTED_MODULE_0__["TrkPt"](t);
            });
        }
        else {
            this.trkpt = undefined;
        }
    }
}


/***/ }),

/***/ "./src/utils/dropzone.ts":
/*!*******************************!*\
  !*** ./src/utils/dropzone.ts ***!
  \*******************************/
/*! exports provided: dropZone */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dropZone", function() { return dropZone; });
class DropZone {
    constructor(container) {
        this.container = container;
        this.container.addEventListener('dragover', this.dragOverHandler.bind(this), false);
        this.container.addEventListener('drop', this.dropHandler.bind(this), false);
        this.promise = new Promise((resolved, rejected) => {
            this.resolved = resolved;
            this.rejected = rejected;
        });
    }
    dropHandler(ev) {
        console.log('File(s) dropped');
        // Prevent default behavior (Prevent file from being opened)
        ev.preventDefault();
        if (ev.dataTransfer.items) {
            // Use DataTransferItemList interface to access the file(s)
            for (let i = 0; i < ev.dataTransfer.items.length; i++) {
                // If dropped items aren't files, reject them
                if (ev.dataTransfer.items[i].kind === 'file') {
                    const file = ev.dataTransfer.items[i].getAsFile();
                    this.resolveFileContent(file);
                    console.log(`... file[${i}].name = ${file.name}`);
                }
            }
        }
        else {
            // Use DataTransfer interface to access the file(s)
            for (let i = 0; i < ev.dataTransfer.files.length; i++) {
                const file = ev.dataTransfer.files[i];
                this.resolveFileContent(file);
                console.log(`... file[${i}].name = ${file.name}`);
            }
        }
        // Pass event to removeDragData for cleanup
        this.removeDragData(ev);
    }
    resolveFileContent(file) {
        const read = new FileReader();
        read.readAsBinaryString(file);
        read.onloadend = () => this.resolved({
            data: read.result,
            file
        });
        read.onerror = (error) => this.rejected(error);
    }
    dragOverHandler(ev) {
        console.log('File(s) in drop zone');
        // Prevent default behavior (Prevent file from being opened)
        ev.preventDefault();
    }
    removeDragData(ev) {
        console.log('Removing drag data');
        if (ev.dataTransfer.items) {
            // Use DataTransferItemList interface to remove the drag data
            ev.dataTransfer.items.clear();
        }
        else {
            // Use DataTransfer interface to remove the drag data
            ev.dataTransfer.clearData();
        }
    }
}
function dropZone(container) {
    const dz = new DropZone(container);
    return dz.promise;
}


/***/ }),

/***/ "./src/utils/file.ts":
/*!***************************!*\
  !*** ./src/utils/file.ts ***!
  \***************************/
/*! exports provided: save, load */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "save", function() { return save; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "load", function() { return load; });
function save(data, filename, type = 'text/xml') {
    const file = new Blob([data], { type });
    if (window.navigator.msSaveOrOpenBlob) { // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    }
    else { // Others
        const a = document.createElement('a');
        const url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}
function load(file, callback) {
    const url = 'YOUR URL HERE';
    const formData = new FormData();
    formData.append('file', file);
    fetch(url, {
        body: formData,
        method: 'POST'
    })
        .then(() => callback())
        .catch(() => console.log('coskaj plane'));
}


/***/ }),

/***/ "./src/utils/utils.ts":
/*!****************************!*\
  !*** ./src/utils/utils.ts ***!
  \****************************/
/*! exports provided: XMLElementToObject, splitTrksegByTime, js2node */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "XMLElementToObject", function() { return XMLElementToObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "splitTrksegByTime", function() { return splitTrksegByTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "js2node", function() { return js2node; });
/* harmony import */ var models_TrkSeg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! models/TrkSeg */ "./src/models/TrkSeg.ts");

// tslint:disable:interface-name
// tslint:disable:prefer-for-of
/*
interface HTMLCollection {
  [Symbol.iterator](): IterableIterator<Element>;
}
interface NamedNodeMap {
  [Symbol.iterator](): IterableIterator<Element>;
}
*/
function sameChildrenTag(item) {
    const set = new Set();
    for (let i = 0; i < item.children.length; i++) {
        const element = item.children.item(i);
        set.add(element.tagName);
    }
    return ((item.children.length > 1) && (set.size === 1));
}
function XMLElementToObject(item) {
    const hasAttributes = item.attributes.length > 0;
    const hasChildren = item.childElementCount > 0;
    const out = {
        [item.nodeName]: {}
    };
    if (hasAttributes) {
        out[item.nodeName] = hasChildren
            ? {}
            : { _value: item.innerHTML };
        for (let i = 0; i < item.attributes.length; i++) {
            const attr = item.attributes.item(i);
            out[item.nodeName][`@${attr.name}`] = attr.value;
        }
    }
    if (hasChildren) {
        const isSameChild = sameChildrenTag(item);
        for (let i = 0; i < item.children.length; i++) {
            const child = item.children.item(i);
            const parsedChild = XMLElementToObject(child)[child.tagName];
            if (isSameChild) {
                if (typeof out[item.nodeName][child.tagName] === 'undefined') {
                    out[item.nodeName][child.tagName] = [];
                }
                out[item.nodeName][child.tagName].push(parsedChild);
            }
            else {
                out[item.nodeName][child.tagName] = parsedChild;
            }
        }
    }
    else if (!hasAttributes) {
        out[item.nodeName] = item.innerHTML;
    }
    return out;
}
function trkptDiff(point1, point2) {
    return point2.time.getTime() - point1.time.getTime();
}
function findTimeSplits(seg, treshold) {
    const splits = [];
    for (let i = 0; i < (seg.trkpt.length - 1); i++) {
        if (trkptDiff(seg.trkpt[i], seg.trkpt[i + 1]) > treshold) {
            splits.push(i + 1);
        }
    }
    return splits;
}
function splitTrksegByTime(seg, treshold = 3600000) {
    let segments = [seg];
    const splits = findTimeSplits(seg, treshold);
    if (splits.length) {
        let start = 0;
        splits.push(seg.trkpt.length);
        segments = splits.map((index) => {
            const tmp = new models_TrkSeg__WEBPACK_IMPORTED_MODULE_0__["TrkSeg"](undefined);
            tmp.trkpt = seg.trkpt.slice(start, index);
            start = index;
            return tmp;
        });
    }
    return segments;
}
function js2node(node, name) {
    const newTrkpt = document.createElement(name);
    if (typeof node === 'object') {
        for (const item in node) {
            if (node.hasOwnProperty(item)) {
                if (item[0] === '@') {
                    newTrkpt.setAttribute(item.slice(1), node[item].toString());
                }
                else if (typeof node[item] === 'object' && (node[item] instanceof Date === false)) {
                    newTrkpt.appendChild(js2node(node[item], item));
                }
                else {
                    const newItem = document.createElement(item);
                    newItem.innerText = node[item] instanceof Date
                        ? node[item].toISOString()
                        : node[item].toString();
                    newTrkpt.appendChild(newItem);
                }
            }
        }
    }
    else {
        newTrkpt.innerText = node;
    }
    return newTrkpt;
}


/***/ }),

/***/ "./src/views/result.ts":
/*!*****************************!*\
  !*** ./src/views/result.ts ***!
  \*****************************/
/*! exports provided: drawResult */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "drawResult", function() { return drawResult; });
function drawResult(trkSegs, fileName) {
    let html = '';
    let i = 0;
    trkSegs.forEach((trkSeg) => {
        html += drawRow(`${fileName}.seg${i}.gpx`, trkSeg.trkpt.length, i);
        i++;
    });
    html = `<table class="aligner__xs--center aligner__xs--middle result">
    <thead>${drawHeader()}</thead>
    <tbody>${html}</tbody>
    </table>`;
    document.getElementById('result').innerHTML = html;
}
function drawRow(fileName, size, segmentId) {
    return `<tr class="result__row">
      <td class="result__item result__item--name">${fileName}</td>
      <td class="result__item result__item--size">${size}</td>
      <td class="result__action">
        <button class="button" data-segment="${segmentId}" data-file="${fileName}"> Download </button>
      </td>
    </tr>`;
}
function drawHeader() {
    return `<tr class="result__header">
      <th class="result__item result__item--name">File name</th>
      <th class="result__item result__item--size">Track points</th>
      <th class="result__action">Action</td>
    </tr>`;
}


/***/ })

/******/ });
//# sourceMappingURL=gpx-split.js.map