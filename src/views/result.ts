import { TrkSeg } from 'models/TrkSeg';

export function drawResult(trkSegs: TrkSeg[], fileName: string): void {
  let html = '';
  let i = 0;

  trkSegs.forEach((trkSeg) => {
    html += drawRow(`${fileName}.seg${i}.gpx`, trkSeg.trkpt.length, i);
    i++;
  });

  html = `<table class="aligner__xs--center aligner__xs--middle result">
    <thead>${drawHeader()}</thead>
    <tbody>${html}</tbody>
    </table>`;

  document.getElementById('result').innerHTML = html;
}

function drawRow(fileName: string, size: number, segmentId: number): string {
  return `<tr class="result__row">
      <td class="result__item result__item--name">${fileName}</td>
      <td class="result__item result__item--size">${size}</td>
      <td class="result__action">
        <button class="button result__download" data-segment="${segmentId}" data-file="${fileName}"> Download </button>
      </td>
    </tr>`;
}

function drawHeader(): string {
  return `<tr class="result__header">
      <th class="result__item result__item--name">File name</th>
      <th class="result__item result__item--size">Track points</th>
      <th class="result__action">Action</td>
    </tr>`;
}
