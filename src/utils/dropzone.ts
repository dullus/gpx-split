interface IResult {
  data: string; // | PromiseLike<string>;
  file: File;
}

class DropZone {
  public promise: Promise<IResult>;

  private container: HTMLElement;
  // private resolved: (value?: string | PromiseLike<string>, file?: File) => void;
  private resolved: (value?: IResult) => void;
  private rejected: (reason?: any) => void;

  constructor(container: HTMLElement) {
    this.container = container;
    this.container.addEventListener('dragover', this.dragOverHandler.bind(this), false);
    this.container.addEventListener('drop', this.dropHandler.bind(this), false);

    this.promise = new Promise<IResult>((resolved, rejected) => {
      this.resolved = resolved;
      this.rejected = rejected;
    });
  }

  private dropHandler(ev: DragEvent): void {
    console.log('File(s) dropped');

    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      for (let i = 0; i < ev.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (ev.dataTransfer.items[i].kind === 'file') {
          const file = ev.dataTransfer.items[i].getAsFile();
          this.resolveFileContent(file);
          console.log(`... file[${i}].name = ${file.name}`);
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      for (let i = 0; i < ev.dataTransfer.files.length; i++) {
        const file = ev.dataTransfer.files[i];
        this.resolveFileContent(file);
        console.log(`... file[${i}].name = ${file.name}`);
      }
    }

    // Pass event to removeDragData for cleanup
    this.removeDragData(ev);
  }

  private resolveFileContent(file: File) {
    const read = new FileReader();
    read.readAsBinaryString(file);
    read.onloadend = () =>
      this.resolved({
        data: read.result as string,
        file
      });
    read.onerror = (error) => this.rejected(error);
  }

  private dragOverHandler(ev: DragEvent): void {
    console.log('File(s) in drop zone');
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  }

  private removeDragData(ev: DragEvent): void {
    console.log('Removing drag data');

    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to remove the drag data
      ev.dataTransfer.items.clear();
    } else {
      // Use DataTransfer interface to remove the drag data
      ev.dataTransfer.clearData();
    }
  }
}

export function dropZone(container: HTMLElement): Promise<IResult> {
  const dz = new DropZone(container);
  return dz.promise;
}
