import { TrkPt } from 'models/TrkPt';
import { TrkSeg } from 'models/TrkSeg';

// tslint:disable:interface-name
// tslint:disable:prefer-for-of
/*
interface HTMLCollection {
  [Symbol.iterator](): IterableIterator<Element>;
}
interface NamedNodeMap {
  [Symbol.iterator](): IterableIterator<Element>;
}
*/

function sameChildrenTag(item: Element): boolean {
  const set = new Set<string>();
  for (let i = 0; i < item.children.length; i++) {
    const element = item.children.item(i);
    set.add(element.tagName);
  }
  return ((item.children.length > 1) && (set.size === 1));
}

interface IObject {
  [key: string]: IObject | IObject[] | string;
}

export function XMLElementToObject(item: Element): IObject {
  const hasAttributes = item.attributes.length > 0;
  const hasChildren = item.childElementCount > 0;
  const out: IObject = {
    [item.nodeName]: {}
  };

  if (hasAttributes) {
    out[item.nodeName] = hasChildren
      ? {}
      : { _value: item.innerHTML };
    for (let i = 0; i < item.attributes.length; i++) {
      const attr = item.attributes.item(i);
      out[item.nodeName][`@${attr.name}`] = attr.value;
    }
  }

  if (hasChildren) {
    const isSameChild = sameChildrenTag(item);
    for (let i = 0; i < item.children.length; i++) {
      const child = item.children.item(i);
      const parsedChild = XMLElementToObject(child)[child.tagName];
      if (isSameChild) {
        if (typeof out[item.nodeName][child.tagName] === 'undefined') {
          out[item.nodeName][child.tagName] = [];
        }
        (out[item.nodeName][child.tagName] as IObject[]).push(parsedChild as IObject);
      } else {
        out[item.nodeName][child.tagName] = parsedChild;
      }
    }
  } else if (!hasAttributes) {
    out[item.nodeName] = item.innerHTML;
  }

  return out;
}

function trkptDiff(point1: TrkPt, point2: TrkPt): number {
  return point2.time.getTime() - point1.time.getTime();
}

function findTimeSplits(seg: TrkSeg, treshold): number[] {
  const splits: number[] = [];
  for (let i = 0; i < (seg.trkpt.length - 1); i++) {
    if (trkptDiff(seg.trkpt[i], seg.trkpt[i + 1]) > treshold) {
      splits.push(i + 1);
    }
  }
  return splits;
}

export function splitTrksegByTime(seg: TrkSeg, treshold: number = 3600000): TrkSeg[] {
  let segments: TrkSeg[] = [seg];
  const splits = findTimeSplits(seg, treshold);
  if (splits.length) {
    let start = 0;
    splits.push(seg.trkpt.length);
    segments = splits.map((index) => {
      const tmp = new TrkSeg(undefined);
      tmp.trkpt = seg.trkpt.slice(start, index);
      start = index;
      return tmp;
    });
  }

  return segments;
}

export function js2node(node: unknown, name: string): HTMLElement {
  const newTrkpt = document.createElement(name);

  if (typeof node === 'object') {
    for (const item in node) {
      if (node.hasOwnProperty(item)) {
        if (item[0] === '@') {
          newTrkpt.setAttribute(item.slice(1), node[item].toString());
        } else if (typeof node[item] === 'object' && (node[item] instanceof Date === false)) {
          newTrkpt.appendChild(js2node(node[item], item));
        } else {
          const newItem = document.createElement(item);
          newItem.innerText = node[item] instanceof Date
            ? node[item].toISOString()
            : node[item].toString();
          newTrkpt.appendChild(newItem);
        }
      }
    }
  } else {
    newTrkpt.innerText = (node as string);
  }
  return newTrkpt;
}

export function castNumber(data: string): number {
  let out = parseFloat(data);
  if (isNaN(out)) {
    out = undefined;
  }
  return out;
}
