
export function save(data: string, filename: string, type = 'text/xml'): void {
  const file = new Blob([data], {type});
  if (window.navigator.msSaveOrOpenBlob) { // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename);
  } else { // Others
    const a = document.createElement('a');
    const url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  }
}

export function load(file: string, callback) {
  const url = 'YOUR URL HERE';
  const formData = new FormData();

  formData.append('file', file);

  fetch(url, {
    body: formData,
    method: 'POST'
  })
  .then(() => callback())
  .catch(() => console.log('coskaj plane'));
}
