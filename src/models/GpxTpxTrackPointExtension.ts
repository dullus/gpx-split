// Strava
export interface IGpxTpxTrackPointExtension {
  'gpxtpx:hr'?: string | number;
}

export class GpxTpxTrackPointExtension {
  public 'gpxtpx:hr': number;

  constructor(data: IGpxTpxTrackPointExtension) {
    this['gpxtpx:hr'] = Number(data['gpxtpx:hr']);
  }
}
