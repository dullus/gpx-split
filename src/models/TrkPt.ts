import { INs3TrackPointExtension, Ns3TrackPointExtension } from 'models/Ns3TrackPointExtension';
import { IK2TrackPointExtension, K2TrackPointExtension } from 'models/K2TrackPointExtension';
import { GpxTpxTrackPointExtension, IGpxTpxTrackPointExtension } from 'models/GpxTpxTrackPointExtension';

interface IExtensions {
  'ns3:TrackPointExtension'?: INs3TrackPointExtension;
  'k2:TrackPointExtension'?: IK2TrackPointExtension;
  'gpxtpx:TrackPointExtension'?: IGpxTpxTrackPointExtension;
}

export interface ITrkPt {
  '@lat': string | number;
  '@lon': string | number;
  ele?: string | number;
  extensions?: IExtensions;
  time: string;
}

export class TrkPt {
  public '@lat': number;
  public '@lon': number;
  public ele?: number;
  public extensions?: {
    'ns3:TrackPointExtension'?: Ns3TrackPointExtension;
    'k2:TrackPointExtension'?: K2TrackPointExtension;
    'gpxtpx:TrackPointExtension'?: GpxTpxTrackPointExtension;
  };
  public time: Date;

  constructor(data: ITrkPt) {
    this['@lat'] = Number(data['@lat']);
    this['@lon'] = Number(data['@lon']);
    this.ele = Number(data.ele);
    if (typeof data.extensions !== 'undefined') {
      this.extensions = {};
      this.setExtension(data.extensions);
    }
    this.time = new Date(data.time);
  }

  private setExtension(data: IExtensions): void {
    const prefix = Object.keys(data)[0].split(':')[0];

    let ext = data[`ns3:TrackPointExtension`] as unknown;
    if (prefix === 'ns3' && this.isExtType<INs3TrackPointExtension>(ext, 'ns3:')) {
      this.extensions[`ns3:TrackPointExtension`] = new Ns3TrackPointExtension(ext);
      return;
    }

    ext = data[`k2:TrackPointExtension`] as unknown;
    if (prefix === 'k2' && this.isExtType<IK2TrackPointExtension>(ext, 'k2:')) {
      this.extensions[`k2:TrackPointExtension`] = new K2TrackPointExtension(ext);
      return;
    }

    ext = data[`gpxtpx:TrackPointExtension`] as unknown;
    if (prefix === 'gpxtpx' && this.isExtType<IGpxTpxTrackPointExtension>(ext, 'gpxtpx:')) {
      this.extensions[`gpxtpx:TrackPointExtension`] = new GpxTpxTrackPointExtension(ext);
      return;
    }
  }

  private isExtType<T>(obj: any, prefix: string): obj is T {
    let valid = true;
    valid = valid && !!obj && typeof obj === 'object';
    Object.keys(obj).forEach((key) => { valid = valid && key.includes(prefix); });
    return valid;
  }
}
