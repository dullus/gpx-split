import { ITrkPt, TrkPt } from 'models/TrkPt';

export interface ITrkSeg {
  trkpt: ITrkPt[];
}

export class TrkSeg {
  public trkpt: TrkPt[];

  constructor(data: ITrkSeg) {
    if (data && data.trkpt && Array.isArray(data.trkpt)) {
      this.trkpt = data.trkpt.map((t) => {
        return new TrkPt(t);
      });
    } else {
      this.trkpt = undefined;
    }
  }
}
