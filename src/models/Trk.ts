import { ITrkSeg, TrkSeg } from 'models/TrkSeg';

export interface ITrk {
  name: string;
  type: string;
  trkseg: ITrkSeg;
}

export class Trk {
  public name: string;
  public type: string;
  public trkseg: TrkSeg;

  constructor(data: ITrk) {
    this.name = data.name;
    this.type = data.type;
    this.trkseg = new TrkSeg(data.trkseg);
  }
}
