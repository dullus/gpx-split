// Strava
export interface IK2TrackPointExtension {
  'k2:hr'?: string | number;
  'k2:cadence'?: string | number;
}

export class K2TrackPointExtension {
  public 'k2:hr': number;
  public 'k2:cadence': number;

  constructor(data: IK2TrackPointExtension) {
    this['k2:hr'] = Number(data['k2:hr']);
    this['k2:cadence'] = Number(data['k2:cadence']);
  }
}
