import { castNumber } from 'utils/utils';

// Garmin connect
export interface INs3TrackPointExtension {
  'ns3:atemp'?: string | number;
  'ns3:hr'?: string | number;
  'ns3:cad'?: string | number;
}

export class Ns3TrackPointExtension {
  public 'ns3:atemp': number;
  public 'ns3:hr': number;
  public 'ns3:cad': number;

  constructor(data: INs3TrackPointExtension) {
    Object.entries(data).forEach(([key, val]: string[]) => {
      const nval = castNumber(val);
      if (typeof nval !== 'undefined') {
        this[key] = nval;
      }
    })
  }
}
