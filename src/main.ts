import { Trk } from 'models/Trk';
import { TrkSeg } from 'models/TrkSeg';
import * as utils from 'utils/utils';
// import { xmldata } from '../test/src/fixtures/sample';
import { dropZone } from 'utils/dropzone';
import { save } from 'utils/file';
import { drawResult } from 'views/result';

let result: TrkSeg[];
let gpxDOM: XMLDocument;

dropZone(document.getElementById('drop-zone')).then(
  (data) => {
    console.log(`file: ${data.file.name}`);
    // console.log(data);
    result = processTrk(data.data);
    drawResult(result, stripSuffix(data.file.name));
    assignButtons();
  },
  (error) => {
    console.log('feed failed ' + error.toString());
  }
);

function stripSuffix(fileName: string): string {
  const parts = fileName.split('.');
  parts.splice(-1, 1);
  return parts.join('.');
}

// load XML file and convert to JS
function processTrk(xmldata: string): TrkSeg[] {
  const parser = new DOMParser();
  gpxDOM = parser.parseFromString(xmldata, 'text/xml') as XMLDocument;
  const trkXML = gpxDOM.getElementsByTagName('trk');
  const trkJS = utils.XMLElementToObject(trkXML[0]);
  const trk = new Trk(trkJS.trk as any);
  // split to segments
  const trkSegs = utils.splitTrksegByTime(trk.trkseg);
  // console.log(result);

  return trkSegs;
}

function assignButtons(): void {
  // const buttons: HTMLCollectionOf<HTMLButtonElement> = document.getElementsByClassName('result__download');
  const buttons: HTMLCollectionOf<Element> = document.getElementsByClassName('result__download');
  for (const button of buttons) {
    button.addEventListener('click', (event) => {
      const target = event.currentTarget as HTMLButtonElement;
      const segment = target.dataset.segment;
      const fileName = target.dataset.file;
      saveTrkSeg(result[segment], fileName);
    });
  }
}

function saveTrkSeg(trkSeg: TrkSeg, fileName = 'out.gpx'): void {
  // create new XML document
  const myDOM = new Document(); // @TODO not ideal, adds xmlns
  // clone old GPX structure
  const gpxNode = gpxDOM.getElementsByTagName('gpx')[0];
  const gpxClone = gpxNode.cloneNode(true);
  // add it to new XML document
  myDOM.appendChild(gpxClone);
  // remove cloned trackseg
  myDOM.getElementsByTagName('trkseg')[0].remove();
  // add new empty trackseg
  const newTrkseg = document.createElement('trkseg');
  myDOM.getElementsByTagName('trk')[0].appendChild(newTrkseg);
  // add trackpoints
  trkSeg.trkpt.forEach((trkpt) => {
    newTrkseg.appendChild(utils.js2node(trkpt, 'trkpt'));
  });
  // console.log(myDOM);

  // XML to string
  const xmlString = new XMLSerializer().serializeToString(myDOM);
  save(xmlString, fileName);
  // console.log(result2);
}
